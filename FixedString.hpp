/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_FIXEDSTRING_HPP
#define OCL_GUARD_STRING_FIXEDSTRING_HPP

#include "StringLengthUtility.hpp"
#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fixed length string.
template<typename CharType = char, typename SizeType = std::size_t, std::size_t const string_size = 256U>
class FixedString
{
/// Types and constants.
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef FixedString<char_type, size_type, string_size> fixed_string_type;
    typedef StringLengthUtility<char_type, size_type> string_length_utility_type;

    static constexpr std::size_t SIZE = string_size > 0 ? string_size : 1;
    static constexpr std::size_t MAX_LEN = SIZE - 1;

/// Constructors and copy constructors.
public:
    FixedString()
        : m_length(0)
    {
        m_value[0] = '\0';
    }

    FixedString(char_type const* value)
    {
        Copy(value);
    }

    FixedString(char_type const* value1, char_type const* value2)
    {
        Copy(value1, value2);
    }

    FixedString(char_type const* value1, char_type const* value2, char_type const* value3)
    {
        Copy(value1, value2, value3);
    }

    FixedString(fixed_string_type const& value)
    {
        ::memcpy(m_value, value.m_value, sizeof(m_value));
    }

    FixedString(fixed_string_type&& value)
    {
        Move(value);
    }

    FixedString& operator=(char_type const* value)
    {
        Copy(value);
        return *this;
    }

    FixedString& operator=(fixed_string_type const& value)
    {
        Copy(value);
        return *this;
    }

    FixedString& operator=(fixed_string_type&& value)
    {
        Move(value);
        return *this;
    }

    FixedString& operator+=(char_type const* value)
    {
        Append(value);
        return *this;
    }

    FixedString& operator+=(fixed_string_type const& value)
    {
        Append(value);
        return *this;
    }

    char_type operator[](size_type index) const noexcept
    {
        return (index < GetSize()) ? m_value[index] : static_cast<char_type>(0);
    }

    char_type& operator[](size_type index) noexcept
    {
        return (index < GetSize()) ? m_value[index] : TempChar();
    }

    operator char_type const*() const noexcept
    {
        return m_value;
    }

public:
    /// Length of string stored in string buffer.
    size_type GetLength() const noexcept
    {
        return m_length;
    }

    /// Size of string buffer.
    static constexpr size_type GetSize() noexcept
    {
        return static_cast<size_type>(SIZE);
    }

    char_type const* Ptr() const noexcept
    {
        return m_value;
    }

    char_type* Ptr() noexcept
    {
        return m_value;
    }

    /// Return a reference to the character used by operator[] when the index is out of range.
    /// This can be used to validate the correct use of operator[].
    char_type& TempChar() noexcept
    {
        static char_type temp_char = static_cast<char_type>(0);
        return temp_char;
    }

    /// Set string to empty.
    void Clear()
    {
        m_value[0] = static_cast<char_type>(0);
        m_length = 0;
    }

    /// Set every character in the buffer to '\0'.
    void SecureClear()
    {
        ::memset(m_value, 0, sizeof(m_value));
        m_length = 0;
    }

    /// Copy string value, when not null.
    void Copy(char_type const* value)
    {
        if (value != nullptr)
        {
            size_type len = string_length_utility_type::UnsafeGetLength(value);
            if (len >= SIZE)
            {
                ::memcpy(m_value, value, sizeof(char_type) * MAX_LEN);
                m_value[MAX_LEN] = static_cast<char_type>(0);
                m_length = MAX_LEN;
            }
            else
            {
                ::memcpy(m_value, value, sizeof(char_type) * (len + 1));
                m_length = len;
            }
        }
        else
            Clear();
    }

    void Copy(char_type const* value1, char_type const* value2)
    {
        Copy(value1);
        Append(value2);
    }

    void Copy(char_type const* value1, char_type const* value2, char_type const* value3)
    {
        Copy(value1);
        Append(value2, value3);
    }

    /// Copy the entire string buffer.
    void Copy(fixed_string_type const& value)
    {
        ::memcpy(m_value, value.m_value, sizeof(m_value));
    }

    /// Move the entire string buffer, clearing the value afterwards.
    void Move(fixed_string_type& value)
    {
        ::memcpy(m_value, value.m_value, sizeof(m_value));
        value.Clear();
    }

    void Append(char_type const* value)
    {
        if (value != nullptr)
        {
            size_type value_len = string_length_utility_type::SafeGetLength(value);
            InternalUnsafeAppend(m_value, SIZE, m_length, value, value_len);
        }
    }

    void Append(char_type const* value1, char_type const* value2)
    {
        if (value1 != nullptr)
        {
            size_type value_len = string_length_utility_type::SafeGetLength(value1);
            InternalUnsafeAppend(m_value, SIZE, m_length, value1, value_len);
        }
        if (value2 != nullptr)
        {
            size_type value_len = string_length_utility_type::SafeGetLength(value2);
            InternalUnsafeAppend(m_value, SIZE, m_length, value2, value_len);
        }
    }

    void Append(char_type const* value1, char_type const* value2, char_type const* value3)
    {
        if (value1 != nullptr)
        {
            size_type value_len = string_length_utility_type::SafeGetLength(value1);
            InternalUnsafeAppend(m_value, SIZE, m_length, value1, value_len);
        }

        if (value2 != nullptr)
        {
            size_type value_len = string_length_utility_type::SafeGetLength(value2);
            InternalUnsafeAppend(m_value, SIZE, m_length, value2, value_len);
        }

        if (value3 != nullptr)
        {
            size_type value_len = string_length_utility_type::SafeGetLength(value3);
            InternalUnsafeAppend(m_value, SIZE, m_length, value3, value_len);
        }
    }

/// Static helpers.
private:
    static void InternalUnsafeAppend(char_type* dest,
                                     size_type dest_size,
                                     size_type& dest_len,
                                     char_type const* src,
                                     size_type src_len)
    {
        if (dest_len < dest_size - 1)
        {
            size_type remaining_size = dest_size - dest_len;
            if (src_len >= remaining_size)
            {
                size_type copy_size = remaining_size - 1;
                ::memcpy(dest + dest_len, src, sizeof(char_type) * copy_size);
                dest[copy_size] = static_cast<char_type>(0);
                dest_len += copy_size;
            }
            else
            {
                size_type copy_size = src_len + 1;
                ::memcpy(dest + dest_len, src, sizeof(char_type) * copy_size);
                dest_len += src_len;
            }
        }
    }

/// Data.
private:
    char_type m_value[string_size > 0 ? string_size : 1];
    size_type m_length;
};

} // namespace ocl

#endif // OCL_GUARD_STRING_FIXEDSTRING_HPP

/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_STRINGLIMITS_HPP
#define OCL_GUARD_STRING_STRINGLIMITS_HPP

#include "internal/InternalStringLimits.hpp"
#include "internal/InternalLimits.hpp"
#include "internal/InternalToString.hpp"
#include <cstddef>

namespace ocl
{

template<typename CharType, typename NumberType, typename SizeType = std::size_t>
struct StringLimits
{
public:
    typedef CharType char_type;
    typedef NumberType number_type;
    typedef SizeType size_type;

private:
    static bool const IS_SIGNED = static_cast<NumberType>(-1) < static_cast<NumberType>(0);
    static std::size_t const NUM_BITS  = static_cast<std::size_t>(sizeof(NumberType) * CHAR_BIT);

    typedef InternalStringLimits<char_type, size_type, NUM_BITS, IS_SIGNED> internal_string_limits_type;

public:
    static constexpr char_type const* GetMin() noexcept
    {
        return internal_string_limits_type::GetMin();
    }

    static constexpr char_type const* GetMax() noexcept
    {
        return internal_string_limits_type::GetMax();
    }

    static char_type const* GetMin(number_type& min_value) noexcept
    {
        min_value = InternalLimits<NUM_BITS, IS_SIGNED>::GetMin();
        return internal_string_limits_type::GetMin();
    }

    static char_type const* GetMax(number_type& max_value) noexcept
    {
        max_value = InternalLimits<NUM_BITS, IS_SIGNED>::GetMax();
        return internal_string_limits_type::GetMax();
    }

    static constexpr char_type const* GetOutOfBoundsMin() noexcept
    {
        return internal_string_limits_type::GetOutOfBoundsMin();
    }

    static constexpr char_type const* GetOutOfBoundsMax() noexcept
    {
        return internal_string_limits_type::GetOutOfBoundsMax();
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return internal_string_limits_type::GetMaxCharacters();
    }
};

template<typename SizeType>
struct StringLimits<char, float, SizeType>
{
public:
    typedef char char_type;
    typedef float float_type;
    typedef SizeType size_type;

    static constexpr char_type const* GetMin() noexcept
    {
        return "-3.402823466e+38";
    }

    static constexpr char_type const* GetMax() noexcept
    {
        return "3.402823466e+38";
    }

    static char_type const* GetMin(float_type& min_value) noexcept
    {
        min_value = FLT_MAX * -1.0f;
        return GetMin();
    }

    static char_type const* GetMax(float_type& max_value) noexcept
    {
        max_value = FLT_MAX;
        return GetMax();
    }

    static constexpr char_type const* GetOutOfBoundsMin() noexcept
    {
        return "-3.402823466e+39";
    }

    static constexpr char_type const* GetOutOfBoundsMax() noexcept
    {
        return "3.402823466e+39";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 9U; // enough space for 0.0000000 or 0000000.0
    }
};

template<typename SizeType>
struct StringLimits<char, double, SizeType>
{
public:
    typedef char char_type;
    typedef double float_type;
    typedef SizeType size_type;

    static constexpr char_type const* GetMin() noexcept
    {
        return "-1.7976931348623158e+308";
    }

    static constexpr char_type const* GetMax() noexcept
    {
        return "1.7976931348623158e+308";
    }

    static char_type const* GetMin(float_type& min_value) noexcept
    {
        min_value = DBL_MAX * -1.0;
        return GetMin();
    }

    static char_type const* GetMax(float_type& max_value) noexcept
    {
        max_value = DBL_MAX;
        return GetMax();
    }

    static constexpr char_type const* GetOutOfBoundsMin() noexcept
    {
        return "-1.7976931348623158e+309";
    }

    static constexpr char_type const* GetOutOfBoundsMax() noexcept
    {
        return "1.7976931348623158e+309";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 16U; // enough space for 0.00000000000000 or 00000000000000.0
    }
};

template<typename SizeType>
struct StringLimits<wchar_t, float, SizeType>
{
public:
    typedef wchar_t char_type;
    typedef float float_type;
    typedef SizeType size_type;

    static constexpr char_type const* GetMin() noexcept
    {
        return L"-3.402823466e+38";
    }

    static constexpr char_type const* GetMax() noexcept
    {
        return L"3.402823466e+38";
    }

    static char_type const* GetMin(float_type& min_value) noexcept
    {
        min_value = FLT_MAX * -1.0f;
        return GetMin();
    }

    static char_type const* GetMax(float_type& max_value) noexcept
    {
        max_value = FLT_MAX;
        return GetMax();
    }

    static constexpr char_type const* GetOutOfBoundsMin() noexcept
    {
        return L"-3.402823466e+39";
    }

    static constexpr char_type const* GetOutOfBoundsMax() noexcept
    {
        return L"3.402823466e+39";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 9U; // enough space for 0.0000000 or 0000000.0
    }
};

template<typename SizeType>
struct StringLimits<wchar_t, double, SizeType>
{
public:
    typedef wchar_t char_type;
    typedef double float_type;
    typedef SizeType size_type;

    static constexpr char_type const* GetMin() noexcept
    {
        return L"-1.7976931348623158e+308";
    }

    static constexpr char_type const* GetMax() noexcept
    {
        return L"1.7976931348623158e+308";
    }

    static char_type const* GetMin(float_type& min_value) noexcept
    {
        min_value = DBL_MAX * -1.0;
        return GetMin();
    }

    static char_type const* GetMax(float_type& max_value) noexcept
    {
        max_value = DBL_MAX;
        return GetMax();
    }

    static constexpr char_type const* GetOutOfBoundsMin() noexcept
    {
        return L"-1.7976931348623158e+309";
    }

    static constexpr char_type const* GetOutOfBoundsMax() noexcept
    {
        return L"1.7976931348623158e+309";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 16U; // enough space for 0.00000000000000 or 00000000000000.0
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_STRINGLIMITS_HPP

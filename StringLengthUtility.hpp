/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_STRINGLENGTHUTILITY_HPP
#define OCL_GUARD_STRING_STRINGLENGTHUTILITY_HPP

#include <cstring>
#include <cstddef>
#include <cwchar>

namespace ocl
{

/// Get the length of a string safely, or unsafely (i.e. not checking for null)
/// When the type is not a char const*, char*, wchar_t const* or wchar_t*,
/// then it's a type with a Ptr member function.
template<typename CharType, typename SizeType = std::size_t>
struct StringLengthUtility;

template<typename SizeType>
struct StringLengthUtility<char, SizeType>
{
public:
    typedef char     char_type;
    typedef SizeType size_type;

    /// Get the length of the string, and return 0 if str is null.
    static size_type SafeGetLength(char_type const* str) noexcept
    {
        return static_cast<size_type>(str != nullptr ? ::strlen(str) : 0);
    }

    /// Get the length of the string, and return 0 if str is null.
    template<typename... Types>
    static size_type SafeGetLength(char_type const* str, Types... args) noexcept
    {
        return SafeGetLength(str) + SafeGetLength(args...);
    }

    /// Get the length of the string, and return 0 if str is null.
    static size_type UnsafeGetLength(char_type const* str)
    {
        return ::strlen(str);
    }

    /// Get the length of the string, and return 0 if str is null.
    template<typename... Types>
    static size_type UnsafeGetLength(char_type const* str, Types... args)
    {
        return UnsafeGetLength(str) + UnsafeGetLength(args...);
    }
};

template<typename SizeType>
struct StringLengthUtility<wchar_t, SizeType>
{
public:
    typedef wchar_t  char_type;
    typedef SizeType size_type;

    /// Get the length of the string, and return 0 if str is null.
    static size_type SafeGetLength(char_type const* str) noexcept
    {
        return static_cast<size_type>(str != nullptr ? ::wcslen(str) : 0);
    }

    /// Get the length of the string, and return 0 if str is null.
    template<typename... Types>
    static size_type SafeGetLength(char_type const* str, Types... args) noexcept
    {
        return SafeGetLength(str) + SafeGetLength(args...);
    }

    /// Get the length of the string, and return 0 if str is null.
    static size_type UnsafeGetLength(char_type const* str)
    {
        return ::wcslen(str);
    }

    /// Get the length of the string, and return 0 if str is null.
    template<typename... Types>
    static size_type UnsafeGetLength(char_type const* str, Types... args)
    {
        return UnsafeGetLength(str) + UnsafeGetLength(args...);
    }
};

} //namespace ocl

#endif // OCL_GUARD_STRING_STRINGLENGTHUTILITY_HPP

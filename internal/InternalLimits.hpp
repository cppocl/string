/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALLIMITS_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALLIMITS_HPP

#include <cstdint>
#include <cstddef>

namespace ocl
{

template<int const SizeOfTypeInBits, bool const is_signed>
struct InternalLimits;

template<>
struct InternalLimits<8, true>
{
    static constexpr std::int8_t GetMin() noexcept
    {
        return INT8_MIN;
    }

    static constexpr std::int8_t GetMax() noexcept
    {
        return INT8_MAX;
    }
};

template<>
struct InternalLimits<8, false>
{
    static constexpr std::uint8_t GetMin() noexcept
    {
        return 0U;
    }

    static constexpr std::uint8_t GetMax() noexcept
    {
        return UINT8_MAX;
    }
};

template<>
struct InternalLimits<16, true>
{
    static constexpr std::int16_t GetMin() noexcept
    {
        return INT16_MIN;
    }

    static constexpr std::int16_t GetMax() noexcept
    {
        return INT16_MAX;
    }
};

template<>
struct InternalLimits<16, false>
{
    static constexpr std::uint16_t GetMin() noexcept
    {
        return 0U;
    }

    static constexpr std::uint16_t GetMax() noexcept
    {
        return UINT16_MAX;
    }
};

template<>
struct InternalLimits<32, true>
{
    static constexpr std::int32_t GetMin() noexcept
    {
        return INT32_MIN;
    }

    static constexpr std::int32_t GetMax() noexcept
    {
        return INT32_MAX;
    }
};

template<>
struct InternalLimits<32, false>
{
    static constexpr std::uint32_t GetMin() noexcept
    {
        return 0UL;
    }

    static constexpr std::uint32_t GetMax() noexcept
    {
        return UINT32_MAX;
    }
};

template<>
struct InternalLimits<64, true>
{
    static constexpr std::int64_t GetMin() noexcept
    {
        return INT64_MIN;
    }

    static constexpr std::int64_t GetMax() noexcept
    {
        return INT64_MAX;
    }
};

template<>
struct InternalLimits<64, false>
{
    static constexpr std::uint64_t GetMin() noexcept
    {
        return 0U;
    }

    static constexpr std::uint64_t GetMax() noexcept
    {
        return UINT64_MAX;
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALLIMITS_HPP

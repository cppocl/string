/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALSKIPWHITESPACE_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALSKIPWHITESPACE_HPP

#include <cctype>
#include <cwctype>

namespace ocl
{

template<typename CharType>
struct InternalSkipWhiteSpace;

template<>
struct InternalSkipWhiteSpace<char>
{
    typedef char char_type;

    static char_type const* SkipWhiteSpace(char_type const* str)
    {
        if (str != nullptr)
            while (::isspace(*str))
                ++str;
        return str;
    }
};

template<>
struct InternalSkipWhiteSpace<wchar_t>
{
    typedef wchar_t char_type;

    static char_type const* SkipWhiteSpace(char_type const* str)
    {
        if (str != nullptr)
            while (::iswspace(*str))
                ++str;
        return str;
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALSKIPWHITESPACE_HPP

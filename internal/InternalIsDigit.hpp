/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALISDIGIT_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALISDIGIT_HPP

#include <cctype>
#include <cwctype>

namespace ocl
{

template<typename CharType>
struct InternalIsDigit;

template<>
struct InternalIsDigit<char>
{
    typedef char char_type;

    static bool IsDigit(char ch) noexcept
    {
        bool is_digit;

        switch (ch)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            is_digit = true;
            break;
        default:
            is_digit = false;
        }

        return is_digit;
    }

    static bool IsDigit(char const* str, bool skip_white_space) noexcept
    {
        bool is_digit;
        if (str != nullptr)
        {
            if (skip_white_space)
                while (::isspace(*str))
                    ++str;
            is_digit = IsDigit(*str);
        }
        else
            is_digit = false;
        return is_digit;
    }
};

template<>
struct InternalIsDigit<wchar_t>
{
    typedef wchar_t char_type;

    static bool IsDigit(wchar_t ch) noexcept
    {
        bool is_digit;

        switch (ch)
        {
        case L'0':
        case L'1':
        case L'2':
        case L'3':
        case L'4':
        case L'5':
        case L'6':
        case L'7':
        case L'8':
        case L'9':
            is_digit = true;
            break;
        default:
            is_digit = false;
        }

        return is_digit;
    }

    static bool IsDigit(wchar_t const* str, bool skip_white_space) noexcept
    {
        bool is_digit;
        if (str != nullptr)
        {
            if (skip_white_space)
                while (::iswspace(*str))
                    ++str;
            is_digit = IsDigit(*str);
        }
        else
            is_digit = false;
        return is_digit;
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALISDIGIT_HPP

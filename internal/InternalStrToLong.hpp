/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALSTRTOLONG_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALSTRTOLONG_HPP

#include <cwchar>
#include <climits>
#include <cstdlib>
#include "InternalIsDigit.hpp"
#include "InternalSkipWhiteSpace.hpp"
#include "InternalCharConstants.hpp"

namespace ocl
{

/// Convert string to long, unsigned long, long long or unsigned long long.
template<typename CharType, typename IntType>
struct InternalStrToLong;

template<>
struct InternalStrToLong<char, signed long int>
{
    typedef char char_type;
    typedef signed long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::strtol(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        value = StrToLong(str, base);
        return (value != 0 || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToLong<char, unsigned long int>
{
    typedef char char_type;
    typedef unsigned long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::strtoul(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        str = InternalSkipWhiteSpace<char_type>::SkipWhiteSpace(str);
        bool success;
        if (str != nullptr && *str != InternalCharConstants<char_type>::Minus)
        {
            value = StrToLong(str, base);
            success = (value != 0 || InternalIsDigit<char_type>::IsDigit(*str)) && (errno == 0);
        }
        else
            success = false;
        return success;
    }
};

template<>
struct InternalStrToLong<char, signed long long int>
{
    typedef char char_type;
    typedef signed long long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::strtoll(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        value = StrToLong(str, base);
        return (value != 0 || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToLong<char, unsigned long long int>
{
    typedef char char_type;
    typedef unsigned long long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::strtoull(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        str = InternalSkipWhiteSpace<char_type>::SkipWhiteSpace(str);
        bool success;
        if (str != nullptr && *str != InternalCharConstants<char_type>::Minus)
        {
            value = StrToLong(str, base);
            success = (value != 0 || InternalIsDigit<char_type>::IsDigit(*str)) && (errno == 0);
        }
        else
            success = false;
        return success;
    }
};

template<>
struct InternalStrToLong<wchar_t, signed long int>
{
    typedef wchar_t char_type;
    typedef signed long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::wcstol(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        value = StrToLong(str, base);
        return (value != 0 || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToLong<wchar_t, unsigned long int>
{
    typedef wchar_t char_type;
    typedef unsigned long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::wcstoul(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        str = InternalSkipWhiteSpace<char_type>::SkipWhiteSpace(str);
        bool success;
        if (str != nullptr && *str != InternalCharConstants<char_type>::Minus)
        {
            value = StrToLong(str, base);
            success = (value != 0 || InternalIsDigit<char_type>::IsDigit(*str)) && (errno == 0);
        }
        else
            success = false;
        return success;
    }
};

template<>
struct InternalStrToLong<wchar_t, signed long long int>
{
    typedef wchar_t char_type;
    typedef signed long long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::wcstoll(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        value = StrToLong(str, base);
        return (value != 0 || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToLong<wchar_t, unsigned long long int>
{
    typedef wchar_t char_type;
    typedef unsigned long long int int_type;

    static int_type StrToLong(char_type const* str, int base = 10)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::wcstoull(str, &str_end, base);
    }

    static bool StrToLong(int_type& value, char_type const* str, int base = 10)
    {
        str = InternalSkipWhiteSpace<char_type>::SkipWhiteSpace(str);
        bool success;
        if (str != nullptr && *str != InternalCharConstants<char_type>::Minus)
        {
            value = StrToLong(str, base);
            success = (value != 0 || InternalIsDigit<char_type>::IsDigit(*str)) && (errno == 0);
        }
        else
            success = false;
        return success;
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALSTRTOLONG_HPP

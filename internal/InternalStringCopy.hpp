/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGCOPY_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGCOPY_HPP

#include "InternalStringLength.hpp"
#include <cstddef>
#include <cstring>

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
struct InternalStringCopy
{
    typedef CharType char_type;
    typedef SizeType size_type;

    /// Copy source string to destination string.
    /// Note this is not safe and requires the caller to ensure the destination
    /// is big enough to store the source string.
    static void Copy(char_type* dest, char_type const* src)
    {
        typedef InternalStringLength<char_type, size_type> string_length_type;

        if ((dest != nullptr) && (src != nullptr))
        {
            size_type const size = (string_length_type::GetLength(src) + static_cast<size_type>(1)) *
                                    sizeof(char_type);
            ::memcpy(dest, src, size);
        }
    }

    /// Copy source string to destination string.
    /// Note this is not safe and requires the caller to ensure the destination
    /// is big enough to store the source string.
    /// If the last character copied is not a null character, then the null character is added.
    static void Copy(char_type* dest, char_type const* src, size_type count)
    {
        if ((dest != nullptr) && (src != nullptr) && (count > static_cast<size_type>(0)))
        {
            ::memcpy(dest, src, count * sizeof(char_type));
            if (*(src + static_cast<std::size_t>(count - 1)) != InternalCharConstants<char_type>::Null)
                *(dest + static_cast<std::size_t>(count)) = InternalCharConstants<char_type>::Null;
        }
    }

    /// Copy source string to destination string.
    /// This will be safe, as long as dest_size is correctly representing
    /// the space within the destination string.
    /// If the last character copied is not a null character, then the null character is added,
    /// if the destination string is big enough.
    static void Copy(char_type* dest, size_type dest_size, char_type const* src, size_type src_size)
    {
        if ((dest != nullptr) &&
            (src != nullptr) &&
            (dest_size >= src_size) &&
            (src_size > static_cast<size_type>(0)))
        {
            ::memcpy(dest, src, src_size * sizeof(char_type));
            if ((*(src + static_cast<std::size_t>(src_size - 1)) != InternalCharConstants<char_type>::Null) &&
                (dest_size > src_size))
            {
                *(dest + static_cast<std::size_t>(src_size)) = InternalCharConstants<char_type>::Null;
            }
        }
    }

};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGCOPY_HPP

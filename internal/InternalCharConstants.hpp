/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALCHARCONSTANTS_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALCHARCONSTANTS_HPP

template<typename CharType>
struct InternalCharConstants;

template<>
struct InternalCharConstants<char>
{
    typedef char char_type;

    static char_type const Null     = '\0';
    static char_type const FullStop = '.';
    static char_type const Minus    = '-';
    static char_type const Plus     = '+';
};

template<>
struct InternalCharConstants<wchar_t>
{
    typedef wchar_t char_type;

    static char_type const Null     = L'\0';
    static char_type const FullStop = L'.';
    static char_type const Minus    = L'-';
    static char_type const Plus     = L'+';
};

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALCHARCONSTANTS_HPP

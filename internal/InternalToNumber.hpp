/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALTONUMBER_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALTONUMBER_HPP

#include <cwchar>
#include <climits>
#include <cstdlib>
#include "InternalIsDigit.hpp"
#include "InternalStrToLong.hpp"
#include "InternalStrToFloat.hpp"

namespace ocl
{

template<typename CharType, typename NumericType>
struct InternalToNumber;

template<typename CharType>
struct InternalToNumber<CharType, signed char>
{
    typedef CharType char_type;
    typedef signed char int_type;
    typedef signed long int conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        conversion_type val = static_cast<conversion_type>(0);
        bool success = InternalStrToLong<char_type, conversion_type>::StrToLong(val, str, base) &&
                       (val >= CHAR_MIN) && (val <= CHAR_MAX);
        value = static_cast<int_type>(val);
        return success;
    }
};

template<typename CharType>
struct InternalToNumber<CharType, unsigned char>
{
    typedef CharType char_type;
    typedef unsigned char int_type;
    typedef unsigned long int conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        conversion_type val = static_cast<conversion_type>(0);
        bool success = InternalStrToLong<char_type, conversion_type>::StrToLong(val, str, base) &&
                       (val <= UCHAR_MAX);
        value = static_cast<int_type>(val);
        return success;
    }
};

template<typename CharType>
struct InternalToNumber<CharType, signed short int>
{
    typedef CharType char_type;
    typedef signed short int int_type;
    typedef signed long int conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        conversion_type val = static_cast<conversion_type>(0);
        bool success = InternalStrToLong<char_type, conversion_type>::StrToLong(val, str, base) &&
            (val >= SHRT_MIN) && (val <= SHRT_MAX);
        value = static_cast<int_type>(val);
        return success;
    }
};

template<typename CharType>
struct InternalToNumber<CharType, unsigned short int>
{
    typedef CharType char_type;
    typedef unsigned short int int_type;
    typedef unsigned long int conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        conversion_type val = static_cast<conversion_type>(0);
        bool success = InternalStrToLong<char_type, conversion_type>::StrToLong(val, str, base) &&
            (val <= USHRT_MAX);
        value = static_cast<int_type>(val);
        return success;
    }
};

template<typename CharType>
struct InternalToNumber<CharType, signed int>
{
    typedef CharType char_type;
    typedef signed int int_type;
    typedef signed long int conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        conversion_type val = static_cast<conversion_type>(0);
        bool success = InternalStrToLong<char_type, conversion_type>::StrToLong(val, str, base) &&
            (val >= INT_MIN) && (val <= INT_MAX);
        value = static_cast<int_type>(val);
        return success;
    }
};

template<typename CharType>
struct InternalToNumber<CharType, unsigned int>
{
    typedef CharType char_type;
    typedef unsigned int int_type;
    typedef unsigned long int conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        conversion_type val = static_cast<conversion_type>(0);
        bool success = InternalStrToLong<char_type, conversion_type>::StrToLong(val, str, base) &&
            (val <= UINT_MAX);
        value = static_cast<int_type>(val);
        return success;
    }
};

template<typename CharType>
struct InternalToNumber<CharType, signed long int>
{
    typedef CharType char_type;
    typedef signed long int int_type;
    typedef int_type conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        value = static_cast<int_type>(0);
        return InternalStrToLong<char_type, conversion_type>::StrToLong(value, str, base);
    }
};

template<typename CharType>
struct InternalToNumber<CharType, unsigned long int>
{
    typedef CharType char_type;
    typedef unsigned long int int_type;
    typedef int_type conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        value = static_cast<int_type>(0);
        return InternalStrToLong<char_type, conversion_type>::StrToLong(value, str, base);
    }
};

template<typename CharType>
struct InternalToNumber<CharType, signed long long int>
{
    typedef CharType char_type;
    typedef signed long long int int_type;
    typedef int_type conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        value = static_cast<int_type>(0);
        return InternalStrToLong<char_type, conversion_type>::StrToLong(value, str, base);
    }
};

template<typename CharType>
struct InternalToNumber<CharType, unsigned long long int>
{
    typedef CharType char_type;
    typedef unsigned long long int int_type;
    typedef int_type conversion_type;

    static bool ToNumber(int_type& value, char_type const* str, int base = 10)
    {
        value = static_cast<int_type>(0);
        return InternalStrToLong<char_type, conversion_type>::StrToLong(value, str, base);
    }
};

template<typename CharType>
struct InternalToNumber<CharType, float>
{
    typedef CharType char_type;
    typedef float float_type;
    typedef float_type conversion_type;

    static bool ToNumber(float_type& value, char_type const* str, int /*base*/)
    {
        value = 0.0f;
        return InternalStrToFloat<char_type, conversion_type>::StrToFloat(value, str);
    }
};

template<typename CharType>
struct InternalToNumber<CharType, double>
{
    typedef CharType char_type;
    typedef double float_type;
    typedef float_type conversion_type;

    static bool ToNumber(float_type& value, char_type const* str, int /*base*/)
    {
        value = 0.0;
        return InternalStrToFloat<char_type, conversion_type>::StrToFloat(value, str);
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALTONUMBER_HPP

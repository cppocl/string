/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALTOSTRING_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALTOSTRING_HPP

#include "InternalStringLimits.hpp"
#include "InternalPrintfFormat.hpp"
#include <cwchar>
#include <cstdio>
#include <cwchar>
#include <climits>
#include <cstddef>

namespace ocl
{

/// Note this is not yet tested, until reported compiler crash for Visual Studio 2015/2017 is fixed.
template<typename CharType, typename NumericType, typename SizeType = std::size_t>
struct InternalToString;

template<typename NumericType, typename SizeType>
struct InternalToString<char, NumericType, SizeType>
{
    typedef char char_type;
    typedef SizeType size_type;
    typedef NumericType numeric_type;

    static std::size_t const size_in_bits = sizeof(numeric_type) * CHAR_BIT;
    static bool const is_signed = static_cast<numeric_type>(-1) < static_cast<numeric_type>(0);

    typedef InternalStringLimits<char_type, size_type, size_in_bits, is_signed> internal_string_limits_type;
    typedef InternalPrintFormat<char_type, numeric_type> internal_print_format_type;

public:
    /// Get the number of characters required to strong the biggest number for conversion.
    static constexpr size_type GetMaxCharacters() noexcept
    {
        return static_cast<size_type>(internal_string_limits_type::GetMaxCharacters());
    }

    /// Get the format string for the snprintf, e.g. "%d".
    static constexpr char_type const* GetFormatString() noexcept
    {
        return internal_print_format_type::GetFormatString();
    }

    /// Store the converted value into the string, specifying the size of the string in characters.
    static bool ToString(char_type* str, SizeType str_size, NumericType  value)
    {
        bool success;

        if ((str != nullptr) && (str_size > 0))
        {
            int ret = snprintf(str, str_size, GetFormatString(), value);
            success = (ret > 0) && (static_cast<size_type>(ret) <= str_size);
        }
        else
            success = false;

        return success;
    }
};

template<typename NumericType, typename SizeType>
struct InternalToString<wchar_t, NumericType, SizeType>
{
    typedef wchar_t char_type;
    typedef SizeType size_type;
    typedef NumericType numeric_type;

    static std::size_t const size_in_bits = sizeof(numeric_type) * CHAR_BIT;
    static bool const is_signed = static_cast<numeric_type>(-1) < static_cast<numeric_type>(0);

    typedef InternalStringLimits<char_type, size_type, size_in_bits, is_signed> internal_string_limits_type;

public:
    static constexpr size_type GetMaxCharacters() noexcept
    {
        return static_cast<size_type>(internal_string_limits_type::GetMaxCharacters());
    }

    static constexpr char_type const* GetFormatString() noexcept
    {
        return internal_string_limits_type::GetFormatString();
    }

    static bool ToString(char_type* str, size_type str_size, numeric_type value)
    {
        bool success;

        if ((str != nullptr) && (str_size > 0))
        {
            int ret = swprintf(str, str_size, GetFormatString(), value);
            success = (ret > 0) && (static_cast<size_type>(ret) <= str_size);
        }
        else
            success = false;

        return success;
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALTOSTRING_HPP

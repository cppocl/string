/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGLENGTH_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGLENGTH_HPP

#include <cstring>
#include <cstddef>
#include <cwchar>

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
struct InternalStringLength;

template<typename SizeType>
struct InternalStringLength<char, SizeType>
{
    typedef char     char_type;
    typedef SizeType size_type;

    static size_type GetLength(char const* str) noexcept
    {
        return static_cast<size_type>(str != nullptr ? ::strlen(str) : 0);
    }
};

template<typename SizeType>
struct InternalStringLength<wchar_t, SizeType>
{
    typedef wchar_t  char_type;
    typedef SizeType size_type;

    static size_type GetLength(wchar_t const* str) noexcept
    {
        return static_cast<size_type>(str != nullptr ? ::wcslen(str) : 0);
    }
};

} //namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGLENGTH_HPP

/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGLIMITS_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGLIMITS_HPP

#include <cstddef>

namespace ocl
{

template<typename CharType, typename SizeType, std::size_t const SizeOfTypeInBits, bool const is_signed>
struct InternalStringLimits;

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 8U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    /// Get the smallest number as a string.
    static constexpr char const* GetMin() noexcept
    {
        return "-128";
    }

    /// Get the largest number as a string.
    static constexpr char const* GetMax() noexcept
    {
        return "127";
    }

    /// Get the smallest number - 1 as a string,
    /// which can be used to test values too small.
    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-129";
    }

    /// Get the largest number + 1 as a string.
    /// which can be used to test values too big.
    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "128";
    }

    /// Get the number of characters required to strong the biggest number for conversion.
    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 4U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 8U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "0";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "255";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-1";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "256";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 3U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 16U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "-32768";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "32767";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-32769";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "32768";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 6U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 16U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "0";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "65535";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-1";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "65536";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 5U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 32U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "-2147483648";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "2147483647";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-2147483649";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "2147483648";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 11U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 32U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "0";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "4294967295";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-1";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "4294967296";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 10U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 64U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "-9223372036854775808";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "9223372036854775807";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-9223372036854775809";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "9223372036854775808";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 20U;
    }
};

template<typename SizeType>
struct InternalStringLimits<char, SizeType, 64U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr char const* GetMin() noexcept
    {
        return "0";
    }

    static constexpr char const* GetMax() noexcept
    {
        return "18446744073709551615";
    }

    static constexpr char const* GetOutOfBoundsMin() noexcept
    {
        return "-1";
    }

    static constexpr char const* GetOutOfBoundsMax() noexcept
    {
        return "18446744073709551616";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 20U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 8U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"-128";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"127";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-129";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"128";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 4U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 8U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"0";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"255";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-1";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"256";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 3U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 16U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"-32768";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"32767";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-32769";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"32768";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 6U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 16U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"0";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"65535";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-1";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"65536";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 5U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 32U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"-2147483648";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"2147483647";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-2147483649";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"2147483648";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 11U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 32U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"0";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"4294967295";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-1";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"4294967296";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 10U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 64U, true>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"-9223372036854775808";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"9223372036854775807";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-9223372036854775809";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"9223372036854775808";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 20U;
    }
};

template<typename SizeType>
struct InternalStringLimits<wchar_t, SizeType, 64U, false>
{
    typedef char char_type;
    typedef SizeType size_type;

    static constexpr wchar_t const* GetMin() noexcept
    {
        return L"0";
    }

    static constexpr wchar_t const* GetMax() noexcept
    {
        return L"18446744073709551615";
    }

    static constexpr wchar_t const* GetOutOfBoundsMin() noexcept
    {
        return L"-1";
    }

    static constexpr wchar_t const* GetOutOfBoundsMax() noexcept
    {
        return L"18446744073709551616";
    }

    static constexpr std::size_t GetMaxCharacters() noexcept
    {
        return 20U;
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALSTRINGLIMITS_HPP

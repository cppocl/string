/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_INTERNAL_INTERNALSTRTOFLOAT_HPP
#define OCL_GUARD_STRING_INTERNAL_INTERNALSTRTOFLOAT_HPP

#include <cwchar>
#include <climits>
#include <cstdlib>
#include "InternalIsDigit.hpp"
#include "InternalSkipWhiteSpace.hpp"

namespace ocl
{

/// Convert string to float or double.
template<typename CharType, typename FloatType>
struct InternalStrToFloat;

template<>
struct InternalStrToFloat<char, float>
{
    typedef char char_type;
    typedef float float_type;

    static float_type StrToFloat(char_type const* str)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::strtof(str, &str_end);
    }

    static bool StrToFloat(float_type& value, char_type const* str)
    {
        value = StrToFloat(str);
        return (value != 0.0f || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToFloat<char, double>
{
    typedef char char_type;
    typedef double float_type;

    static float_type StrToFloat(char_type const* str)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::strtod(str, &str_end);
    }

    static bool StrToFloat(float_type& value, char_type const* str)
    {
        value = StrToFloat(str);
        return (value != 0.0 || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToFloat<wchar_t, float>
{
    typedef wchar_t char_type;
    typedef float float_type;

    static float_type StrToFloat(char_type const* str)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::wcstof(str, &str_end);
    }

    static bool StrToFloat(float_type& value, char_type const* str)
    {
        value = StrToFloat(str);
        return (value != 0.0f || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

template<>
struct InternalStrToFloat<wchar_t, double>
{
    typedef wchar_t char_type;
    typedef double float_type;

    static float_type StrToFloat(char_type const* str)
    {
        char_type* str_end = nullptr;
        errno = 0;
        return std::wcstod(str, &str_end);
    }

    static bool StrToFloat(float_type& value, char_type const* str)
    {
        value = StrToFloat(str);
        return (value != 0.0 || InternalIsDigit<char_type>::IsDigit(str, true)) && (errno == 0);
    }
};

} // namespace ocl

#endif // OCL_GUARD_STRING_INTERNAL_INTERNALSTRTOFLOAT_HPP

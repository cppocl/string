/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_STRING_STRINGUTILITY_HPP
#define OCL_GUARD_STRING_STRINGUTILITY_HPP

#include "internal/InternalStringLength.hpp"
#include "internal/InternalToNumber.hpp"
#include "internal/InternalToString.hpp"
#include "internal/InternalLimits.hpp"
#include "internal/InternalSkipWhiteSpace.hpp"
#include "internal/InternalCharConstants.hpp"
#include "internal/InternalStringCopy.hpp"
#include "internal/InternalPrintfFormat.hpp"
#include "StringLimits.hpp"

namespace ocl
{

/// char and wchar_t pointer (string) utility functions.
/// All pointers are tested against null before modification.
template<typename CharType, typename SizeType = std::size_t>
struct StringUtility
{
    typedef CharType char_type;
    typedef SizeType size_type;

    /// Get the minimum value of the integer type, as a string.
    /// e.g. signed char would be "-128".
    template<typename IntType>
    static constexpr char_type const* GetMin() noexcept;

    /// Get the maximum value of the integer type, as a string.
    /// e.g. signed char would be "127"
    template<typename IntType>
    static constexpr char_type const* GetMax() noexcept;

    /// Get the minimum value of the integer type, as a string.
    /// e.g. signed char would be "-128".
    /// Also returns the minimum value as an integer value.
    template<typename IntType>
    static char_type const* GetMin(IntType& min_value) noexcept;

    /// Get the maximum value of the integer type, as a string.
    /// e.g. signed char would be "127"
    /// Also returns the maximum value as an integer value.
    template<typename IntType>
    static char_type const* GetMax(IntType& max_value) noexcept;

    /// Get the minimum value - 1 of the integer type, as a string.
    /// e.g. signed char would be "-129"
    template<typename IntType>
    static constexpr char_type const* GetOutOfBoundsMin() noexcept;

    /// Get the maximum value + 1 of the integer type, as a string.
    /// e.g. signed char would be "128"
    template<typename IntType>
    static constexpr char_type const* GetOutOfBoundsMax() noexcept;

    /// Calculate the length of the string, and return characters up to '\0',
    /// or return 0 if str is null.
    static size_type GetLength(char_type const* str) noexcept;

    /// Calculate the length of all strings combined, and return characters up to '\0',
    /// or return 0 if str is null.
    template<typename... Types>
    static size_type GetLength(char_type const* first, Types... args) noexcept;

    /// Copy source string to destination string.
    /// Note this is not safe and requires the caller to ensure the destination
    /// is big enough to store the source string.
    static void Copy(char_type* dest, char_type const* src);

    /// Copy source string to destination string.
    /// Note this is not safe and requires the caller to ensure the destination
    /// is big enough to store the source string.
    /// If the last character copied is not a null character, then the null character is added.
    static void Copy(char_type* dest, char_type const* src, size_type count);

    /// Copy source string to destination string.
    /// This will be safe, as long as dest_size is correctly representing
    /// the space within the destination string.
    /// If the last character copied is not a null character, then the null character is added,
    /// if the destination string is big enough.
    static void Copy(char_type* dest, size_type dest_size, char_type const* src, size_type src_size);

    /// Skip all white space and return pointer to first non-white space character.
    char_type const* SkipWhiteSpace(char_type const* str);

    /// Convert a string tp integer or floating point types.
    /// Integer types can convert base 2 to 36, as is supported by std::strtol.
    /// floating point ignores base, and long double is not supported.
    template<typename NumericType>
    static bool ToNumber(NumericType& value, char_type const* str, int base = 10);

#if 0
    /// ToString function crashes the Visual Studio 2015 and 2017 compilers, so this won't
    /// be available until Microsoft fix the problem. This has been reported for at least 2016,
    /// and seems to be related to constexpr.

    /// Convert integer and floating point types to a string.
    template<typename NumericType>
    static bool ToString(char_type* str, size_type str_size, NumericType value);
#endif
};

#include "StringUtility.inl"

} // namespace ocl

#endif // OCL_GUARD_STRING_STRINGUTILITY_HPP

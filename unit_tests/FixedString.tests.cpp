/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../FixedString.hpp"
#include <cstddef>
#include <string>
#include <cstdlib>
#include <ctime>

char const* strings[] = { "ABC", "DEF", "GHI", "JKL" };

TEST_SETUP(FixedString)
{
    ::srand(static_cast<unsigned int>(time(nullptr)));
}

char const* GetStr()
{
    return strings[rand() & 3];
}

TEST_MEMBER_FUNCTION(FixedString, constructor, NA)
{
    TEST_OVERRIDE_FUNCTION_NAME("FixedString");

    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_EQUAL(str.GetLength(), 0U);
}

TEST_MEMBER_FUNCTION(FixedString, constructor, char_const_ptr)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("FixedString", "char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str(nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 256> str("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str("A");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str("AB");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, constructor, FixedString)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("FixedString", "FixedString const&");

    {
        typedef ocl::FixedString<char, std::size_t, 256> string_type;

        string_type str(string_type(nullptr));
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 256> str("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str("A");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str("AB");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, operator_equal, char_const_ptr)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;
        str = "";
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 256> str;
        str = "A";
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;
        str = "AB";
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, operator_plus_equal, char_const_ptr)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;
        str = "";
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
        str += "";
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
        str += "A";
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 256> str;
        str += "A";
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
        str += "B";
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "AB") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;
        str += "A";
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
        str += "B";
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, Copy, char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;

        str.Copy(nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("A");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Copy("BC");
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "BC") == 0);

        str.Copy("D");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Copy("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;

        str.Copy(nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("A");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Copy("BC");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "B") == 0);

        str.Copy("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, Copy, char_const_ptr_char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char const*, char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;

        str.Copy(nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy(nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("A", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Copy("", "a");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Copy("B", "C");
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "BC") == 0);

        str.Copy("D", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Copy("", "d");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Copy("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;

        str.Copy(nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy(nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("A", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Copy("", "a");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Copy("B", "C");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "B") == 0);

        str.Copy("D", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Copy("", "d");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Copy("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, Copy, char_const_ptr_char_const_ptr_char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char const*, char const*, char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;

        str.Copy(nullptr, nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy(nullptr, "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", "", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("A", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Copy("", "a", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Copy("B", "C", "");
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "BC") == 0);

        str.Copy("E", "F", "G");
        CHECK_EQUAL(str.GetLength(), 3U);
        CHECK_TRUE(StrCmp(str.Ptr(), "EFG") == 0);

        str.Copy("D", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Copy("", "d", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Copy("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;

        str.Copy(nullptr, nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy(nullptr, "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("", "", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Copy("A", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Copy("", "a", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Copy("B", "C", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "B") == 0);

        str.Copy("E", "F", "G");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "E") == 0);

        str.Copy("D", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Copy("", "d", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Copy("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, Append, char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;

        str.Clear();
        str.Append(nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("A");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Clear();
        str.Append("BC");
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "BC") == 0);

        str.Clear();
        str.Append("D");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Clear();
        str.Append("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;

        str.Clear();
        str.Append(nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("A");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Clear();
        str.Append("BC");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "B") == 0);

        str.Clear();
        str.Append("");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, Append, char_const_ptr_char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char const*, char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;

        str.Clear();
        str.Append(nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append(nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("A", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Clear();
        str.Append("", "a");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Clear();
        str.Append("B", "C");
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "BC") == 0);

        str.Clear();
        str.Append("D", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Clear();
        str.Append("", "d");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Clear();
        str.Append("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;

        str.Clear();
        str.Append(nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append(nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("A", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Clear();
        str.Append("", "a");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Clear();
        str.Append("B", "C");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "B") == 0);

        str.Clear();
        str.Append("D", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Clear();
        str.Append("", "d");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Clear();
        str.Append("", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }
}

TEST_MEMBER_FUNCTION(FixedString, Append, char_const_ptr_char_const_ptr_char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char const*, char const*, char const*");

    {
        ocl::FixedString<char, std::size_t, 256> str;

        str.Clear();
        str.Append(nullptr, nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append(nullptr, "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", "", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("A", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Clear();
        str.Append("", "a", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Clear();
        str.Append("B", "C", "");
        CHECK_EQUAL(str.GetLength(), 2U);
        CHECK_TRUE(StrCmp(str.Ptr(), "BC") == 0);

        str.Clear();
        str.Append("E", "F", "G");
        CHECK_EQUAL(str.GetLength(), 3U);
        CHECK_TRUE(StrCmp(str.Ptr(), "EFG") == 0);

        str.Clear();
        str.Append("D", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Clear();
        str.Append("", "d", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Clear();
        str.Append("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }

    {
        ocl::FixedString<char, std::size_t, 2> str;

        str.Clear();
        str.Append(nullptr, nullptr, nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append(nullptr, "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", nullptr, "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("", "", nullptr);
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);

        str.Clear();
        str.Append("A", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "A") == 0);

        str.Clear();
        str.Append("", "a", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "a") == 0);

        str.Clear();
        str.Append("B", "C", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "B") == 0);

        str.Clear();
        str.Append("E", "F", "G");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "E") == 0);

        str.Clear();
        str.Append("D", "", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "D") == 0);

        str.Clear();
        str.Append("", "d", "");
        CHECK_EQUAL(str.GetLength(), 1U);
        CHECK_TRUE(StrCmp(str.Ptr(), "d") == 0);

        str.Clear();
        str.Append("", "", "");
        CHECK_EQUAL(str.GetLength(), 0U);
        CHECK_TRUE(StrCmp(str.Ptr(), "") == 0);
    }
}

namespace
{
    template<typename CharType, typename SizeType, std::size_t const SIZE>
    void Copy2(ocl::FixedString<CharType, SizeType, SIZE>& str,
        char const* str1,
        char const* str2)
    {
        str.Copy(str1, str2);
    }

    void Copy2(std::string& str, char const* str1, char const* str2)
    {
        str.assign(str1);
        str.append(str2);
    }

    template<typename CharType, typename SizeType, std::size_t const SIZE>
    void Copy3(ocl::FixedString<CharType, SizeType, SIZE>& str,
        char const* str1,
        char const* str2,
        char const* str3)
    {
        str.Copy(str1, str2, str3);
    }

    void Copy3(std::string& str, char const* str1, char const* str2, char const* str3)
    {
        str.assign(str1);
        str.append(str2);
        str.append(str3);
    }

    template<typename CharType, typename SizeType, std::size_t const SIZE>
    void Append2(ocl::FixedString<CharType, SizeType, SIZE>& str,
                 char const* str1,
                 char const* str2)
    {
        str.Append(str1, str2);
    }

    void Append2(std::string& str, char const* str1, char const* str2)
    {
        str.append(str1);
        str.append(str2);
    }

    template<typename CharType, typename SizeType, std::size_t const SIZE>
    void Append3(ocl::FixedString<CharType, SizeType, SIZE>& str,
                     char const* str1,
                     char const* str2,
                     char const* str3)
    {
        str.Append(str1, str2, str3);
    }

    void Append3(std::string& str, char const* str1, char const* str2, char const* str3)
    {
        str.append(str1);
        str.append(str2);
        str.append(str3);
    }
}

TEST_MEMBER_FUNCTION_TIME(FixedString, Copy, char_const_ptr, 0, 100)
{
    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_TIME(str.Copy(GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(std_string, copy, char_const_ptr, 0, 100)
{
    std::string str;
    CHECK_TIME(str.assign(GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(FixedString, Copy, char_const_ptr_char_const_ptr, 0, 100)
{
    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_TIME(Copy2(str, GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(std_string, copy, char_const_ptr_char_const_ptr, 0, 100)
{
    std::string str;
    CHECK_TIME(Copy2(str, GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(FixedString, Copy, char_const_ptr_char_const_ptr_char_const_ptr, 0, 100)
{
    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_TIME(Copy3(str, GetStr(), GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(std_string, copy, char_const_ptr_char_const_ptr_char_const_ptr, 0, 100)
{
    std::string str;
    CHECK_TIME(Copy3(str, GetStr(), GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(FixedString, Append, char_const_ptr, 0, 100)
{
    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_TIME(str.Append(GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(std_string, append, char_const_ptr, 0, 100)
{
    std::string str;
    CHECK_TIME(str.append(GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(FixedString, Append, char_const_ptr_char_const_ptr, 0, 100)
{
    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_TIME(Append2(str, GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(std_string, append, char_const_ptr_char_const_ptr, 0, 100)
{
    std::string str;
    CHECK_TIME(Append2(str, GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(FixedString, Append, char_const_ptr_char_const_ptr_char_const_ptr, 0, 100)
{
    ocl::FixedString<char, std::size_t, 256> str;
    CHECK_TIME(Append3(str, GetStr(), GetStr(), GetStr()));
}

TEST_MEMBER_FUNCTION_TIME(std_string, append, char_const_ptr_char_const_ptr_char_const_ptr, 0, 100)
{
    std::string str;
    CHECK_TIME(Append3(str, GetStr(), GetStr(), GetStr()));
}

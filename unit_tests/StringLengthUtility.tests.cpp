/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../StringLengthUtility.hpp"
#include <cstddef>

TEST_MEMBER_FUNCTION(StringLengthUtility, SafeGetLength, char_const_ptr)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::StringLengthUtility<char_type, size_type> string_length_utility;

    CHECK_EQUAL(string_length_utility::SafeGetLength(""), 0U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("", ""), 0U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("", "", ""), 0U);

    CHECK_EQUAL(string_length_utility::SafeGetLength("A"), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("A", ""), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("", "A"), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("A", "B"), 2U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("A", "", ""), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("", "A", ""), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("", "", "A"), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("A", "", "C"), 2U);
    CHECK_EQUAL(string_length_utility::SafeGetLength("A", "B", "C"), 3U);
}

TEST_MEMBER_FUNCTION(StringLengthUtility, SafeGetLength, wchar_t_const_ptr)
{
    typedef wchar_t char_type;
    typedef std::size_t size_type;
    typedef ocl::StringLengthUtility<char_type, size_type> string_length_utility;

    CHECK_EQUAL(string_length_utility::SafeGetLength(L""), 0U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"", L""), 0U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"", L"", L""), 0U);

    CHECK_EQUAL(string_length_utility::SafeGetLength(L"A"), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"A", L""), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"", L"A"), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"A", L"B"), 2U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"A", L"", L""), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"", L"A", L""), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"", L"", L"A"), 1U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"A", L"", L"C"), 2U);
    CHECK_EQUAL(string_length_utility::SafeGetLength(L"A", L"B", L"C"), 3U);
}

TEST_MEMBER_FUNCTION(StringLengthUtility, UnsafeGetLength, char_const_ptr)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::StringLengthUtility<char_type, size_type> string_length_utility;

    CHECK_EQUAL(string_length_utility::UnsafeGetLength(""), 0U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("", ""), 0U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("", "", ""), 0U);

    CHECK_EQUAL(string_length_utility::UnsafeGetLength("A"), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("A", ""), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("", "A"), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("A", "B"), 2U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("A", "", ""), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("", "A", ""), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("", "", "A"), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("A", "", "C"), 2U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength("A", "B", "C"), 3U);
}

TEST_MEMBER_FUNCTION(StringLengthUtility, UnsafeGetLength, wchar_t_const_ptr)
{
    typedef wchar_t char_type;
    typedef std::size_t size_type;
    typedef ocl::StringLengthUtility<char_type, size_type> string_length_utility;

    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L""), 0U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"", L""), 0U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"", L"", L""), 0U);

    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"A"), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"A", L""), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"", L"A"), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"A", L"B"), 2U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"A", L"", L""), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"", L"A", L""), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"", L"", L"A"), 1U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"A", L"", L"C"), 2U);
    CHECK_EQUAL(string_length_utility::UnsafeGetLength(L"A", L"B", L"C"), 3U);
}

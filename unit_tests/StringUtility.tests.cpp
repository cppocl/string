/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cwchar>
#include <cstddef>
#include <cstring>
#include <cstdint>
#include "../../unit_test_framework/test/Test.hpp"
#include "../StringUtility.hpp"
#include "../StringLimits.hpp"
#include "../internal/InternalLimits.hpp"
#include "../internal/InternalSprintf.hpp"

namespace
{
    std::size_t StrLen(char const* str)
    {
        return ::strlen(str);
    }

    std::size_t StrLen(wchar_t const* str)
    {
        return ::wcslen(str);
    }

    template<typename Type>
    bool Compare(Type const* ptr1, Type const* ptr2, std::size_t size)
    {
        return ::memcmp(ptr1, ptr2, size * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(StringUtility, GetLength, char_const_ptr_dot_dot_dot)
{
    TEST_OVERRIDE_ARGS("char const*...");

    typedef ocl::StringUtility<char, std::size_t> string_utility_type;

    CHECK_EQUAL(string_utility_type::GetLength(nullptr), 0U);
    CHECK_EQUAL(string_utility_type::GetLength(""), 0U);
    CHECK_EQUAL(string_utility_type::GetLength("A"), 1U);
    CHECK_EQUAL(string_utility_type::GetLength(nullptr, nullptr), 0U);
    CHECK_EQUAL(string_utility_type::GetLength("", ""), 0U);
    CHECK_EQUAL(string_utility_type::GetLength("A", ""), 1U);
    CHECK_EQUAL(string_utility_type::GetLength("", "A"), 1U);
    CHECK_EQUAL(string_utility_type::GetLength("A", "B"), 2U);
}

TEST_MEMBER_FUNCTION(StringUtility, Copy, char_ptr_char_const_ptr)
{
    TEST_OVERRIDE_ARGS("char*, const char*");

    typedef char char_type;

    std::size_t const size = 3U;
    char_type str1[size] = {'0', '0', '0'};
    char_type str2[size] = "AB";

    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;

    string_utility_type::Copy(str1, str2);
    CHECK_EQUAL(::memcmp(str1, str2, sizeof(str2)), 0);
}

TEST_MEMBER_FUNCTION(StringUtility, Copy, char_ptr_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("char*, const char*, size_t");

    typedef char char_type;

    std::size_t const size = 3U;
    char_type str1[size] = {'0', '0', '0'};
    char_type str2[size] = "AB";

    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;

    // Copy should add the '\0' to end of str1.
    string_utility_type::Copy(str1, str2, size - 1U);
    CHECK_EQUAL(::memcmp(str1, str2, sizeof(str2)), 0);

    ::memcpy(str1, "000", sizeof(char_type) * size);
    string_utility_type::Copy(str1, str2, size);
    CHECK_EQUAL(::memcmp(str1, str2, sizeof(str2)), 0);
}

TEST_MEMBER_FUNCTION(StringUtility, Copy, char_ptr_size_t_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("char*, size_t, const char*, size_t");

    typedef char char_type;

    std::size_t const size = 3U;
    char_type str1[size] = {'0', '0', '0'};
    char_type str2[size] = "AB";

    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;

    // Copy should add the '\0' to end of str1.
    string_utility_type::Copy(str1, size, str2, size - 1U);
    CHECK_EQUAL(::memcmp(str1, str2, sizeof(str2)), 0);

    ::memcpy(str1, "000", sizeof(char_type) * size);
    string_utility_type::Copy(str1, size, str2, size);
    CHECK_EQUAL(::memcmp(str1, str2, sizeof(str2)), 0);

    // Copy should not add the '\0' to end of str1.
    ::memcpy(str1, "000", sizeof(char_type) * size);
    string_utility_type::Copy(str1, size - 1U, str2, size - 1U);
    CHECK_EQUAL(::memcmp(str1, "AB0", sizeof(str2)), 0);
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_char_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed char numeric_type;

    TEST_OVERRIDE_ARGS("signed char&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_char_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned char numeric_type;

    TEST_OVERRIDE_ARGS("unsigned char&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_short_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed short numeric_type;

    TEST_OVERRIDE_ARGS("signed short&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_short_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned short numeric_type;

    TEST_OVERRIDE_ARGS("unsigned short&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_int_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed int numeric_type;

    TEST_OVERRIDE_ARGS("signed int&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_int_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned int numeric_type;

    TEST_OVERRIDE_ARGS("unsigned short&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2U);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_long_int_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed long int numeric_type;

    TEST_OVERRIDE_ARGS("signed long int&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_long_int_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned long int numeric_type;

    TEST_OVERRIDE_ARGS("unsigned long int&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2U);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_long_long_int_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed long long int numeric_type;

    TEST_OVERRIDE_ARGS("signed long long int&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_long_long_int_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned long long int numeric_type;

    TEST_OVERRIDE_ARGS("unsigned long long int&, const char*");

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, float_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef float numeric_type;

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0.0f);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0.0f);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2.0f);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2.0f);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1.0f;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1.0f;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1.0f;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, double_ref_char_const_ptr_int)
{
    typedef char char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef double numeric_type;

    char_type const* str1 = "0";
    char_type const* str2 = " 0";
    char_type const* str3 = "2";
    char_type const* str4 = "-2";
    char_type const* invalid = "x";

    numeric_type value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0.0);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0.0);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2.0);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2.0);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1.0;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1.0;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1.0;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_char_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed char numeric_type;

    TEST_OVERRIDE_ARGS("signed char&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_char_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned char numeric_type;

    TEST_OVERRIDE_ARGS("unsigned char&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_short_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed short numeric_type;

    TEST_OVERRIDE_ARGS("signed short&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_short_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned short numeric_type;

    TEST_OVERRIDE_ARGS("unsigned short&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_int_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed int numeric_type;

    TEST_OVERRIDE_ARGS("signed int&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_int_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned int numeric_type;

    TEST_OVERRIDE_ARGS("unsigned int&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2U);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_long_int_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed long int numeric_type;

    TEST_OVERRIDE_ARGS("signed long int&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_long_int_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned long int numeric_type;

    TEST_OVERRIDE_ARGS("unsigned long int&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0U);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2U);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, signed_long_long_int_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef signed long long int numeric_type;

    TEST_OVERRIDE_ARGS("signed long long int&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, unsigned_long_long_int_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef unsigned long long int numeric_type;

    TEST_OVERRIDE_ARGS("unsigned long long int&, const wchar_t*");

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, str4));

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, float_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef float numeric_type;

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0.0f);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0.0f);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2.0f);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2.0f);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1.0f;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1.0f;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1.0f;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1.0f;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

TEST_MEMBER_FUNCTION(StringUtility, ToNumber, double_ref_wchar_t_const_ptr_int)
{
    typedef wchar_t char_type;
    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef double numeric_type;

    char_type const* str1 = L"0";
    char_type const* str2 = L" 0";
    char_type const* str3 = L"2";
    char_type const* str4 = L"-2";
    char_type const* invalid = L"x";

    numeric_type value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str1));
    CHECK_EQUAL(value, 0.0);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str2));
    CHECK_EQUAL(value, 0.0);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str3));
    CHECK_EQUAL(value, 2.0);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, str4));
    CHECK_EQUAL(value, -2.0);

    numeric_type min_value;
    numeric_type max_value;
    char_type const* min_str = string_utility_type::GetMin(min_value);
    char_type const* max_str = string_utility_type::GetMax(max_value);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, min_str));
    CHECK_EQUAL(value, min_value);

    value = 1.0;
    CHECK_TRUE(string_utility_type::ToNumber(value, max_str));
    CHECK_EQUAL(value, max_value);

    value = 1.0;
    CHECK_FALSE(string_utility_type::ToNumber(value, invalid));

    char_type const* min_out_of_bounds_str = string_utility_type::GetOutOfBoundsMin<numeric_type>();
    char_type const* max_out_of_bounds_str = string_utility_type::GetOutOfBoundsMax<numeric_type>();

    value = 1.0;
    CHECK_FALSE(string_utility_type::ToNumber(value, min_out_of_bounds_str));

    value = 1.0;
    CHECK_FALSE(string_utility_type::ToNumber(value, max_out_of_bounds_str));
}

#if 0
/// See comments in StringUtility.hpp for details on ToString being disabled.
TEST_MEMBER_FUNCTION(StringUtility, ToString, char_ptr_size_type_signed_char)
{
    TEST_OVERRIDE_ARGS("char*, size_type, signed char");

    typedef char char_type;
    typedef signed char numeric_type;

    bool const is_signed = static_cast<numeric_type>(-1) < static_cast<numeric_type>(0);
    std::size_t const size_in_bits = sizeof(numeric_type) * CHAR_BIT;

    typedef ocl::StringUtility<char_type, std::size_t> string_utility_type;
    typedef ocl::StringLimits<char_type, numeric_type> string_limits_type;
    typedef ocl::InternalLimits<size_in_bits, is_signed> internal_limits_type;
    typedef ocl::InternalSprintf<char_type, numeric_type> internal_sprintf_type;

    std::size_t const str_size = string_limits_type::GetMaxCharacters() + 1;
    char_type str[str_size];

    numeric_type value = internal_limits_type::GetMin();
    //CHECK_TRUE(string_utility_type::ToString(str, str_size, value));
    CHECK_TRUE(internal_sprintf_type::Sprintf(str, str_size, value));
    CHECK_TRUE(Compare(str, string_limits_type::GetMin(), str_size));
}
#endif

# string

![](header_image.jpg)

String utility library for char arrays and char pointer manipulation or conversion.

## Code Examples

```cpp
#include <cstdio>
#include "string/StringUtility.hpp"

int main()
{
    const char* str1 = "1234";
    const char* str2 = "5678";

    int i1 = 0;
    int i2 = 0;
    int total_len = ocl::StringUtility<char, int>::GetLength(str1, str2);

    ocl::StringUtility<char>::ToNumber(i1, str1);
    ocl::StringUtility<char>::ToNumber(i2, str2);

    printf("The total characters for all strings is %d\n", total_len);
    printf("The string \"%s\" has been converted to %d\n", str1, i1);
    printf("The string \"%s\" has been converted to %d\n", str2, i2);
}
```

/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


template<typename CharType, typename SizeType>
template<typename IntType>
constexpr CharType const* StringUtility<CharType, SizeType>::GetMin() noexcept
{
    return StringLimits<CharType, IntType>::GetMin();
}

template<typename CharType, typename SizeType>
template<typename IntType>
constexpr CharType const* StringUtility<CharType, SizeType>::GetMax() noexcept
{
    return StringLimits<CharType, IntType>::GetMax();
}

template<typename CharType, typename SizeType>
template<typename IntType>
CharType const* StringUtility<CharType, SizeType>::GetMin(IntType& min_value) noexcept
{
    return StringLimits<CharType, IntType>::GetMin(min_value);
}

template<typename CharType, typename SizeType>
template<typename IntType>
CharType const* StringUtility<CharType, SizeType>::GetMax(IntType& max_value) noexcept
{
    return StringLimits<CharType, IntType>::GetMax(max_value);
}

template<typename CharType, typename SizeType>
template<typename IntType>
constexpr CharType const* StringUtility<CharType, SizeType>::GetOutOfBoundsMin() noexcept
{
    return StringLimits<CharType, IntType>::GetOutOfBoundsMin();
}

template<typename CharType, typename SizeType>
template<typename IntType>
constexpr CharType const* StringUtility<CharType, SizeType>::GetOutOfBoundsMax() noexcept
{
    return StringLimits<CharType, IntType>::GetOutOfBoundsMax();
}

template<typename CharType, typename SizeType>
SizeType StringUtility<CharType, SizeType>::GetLength(char_type const* str) noexcept
{
    return InternalStringLength<char_type, size_type>::GetLength(str);
}

template<typename CharType, typename SizeType>
template<typename... Types>
SizeType StringUtility<CharType, SizeType>::GetLength(char_type const* first, Types... args) noexcept
{
    size_type len = InternalStringLength<char_type, size_type>::GetLength(first);
    return len + GetLength(static_cast<char_type const*>(args)...);
}

template<typename CharType, typename SizeType>
void StringUtility<CharType, SizeType>::Copy(char_type* dest, char_type const* src)
{
    InternalStringCopy<char_type, size_type>::Copy(dest, src);
}

template<typename CharType, typename SizeType>
void StringUtility<CharType, SizeType>::Copy(char_type* dest, char_type const* src, size_type count)
{
    InternalStringCopy<char_type, size_type>::Copy(dest, src, count);
}

template<typename CharType, typename SizeType>
void StringUtility<CharType, SizeType>::Copy(char_type* dest,
                                             size_type dest_size,
                                             char_type const* src,
                                             size_type src_size)
{
    InternalStringCopy<char_type, size_type>::Copy(dest, dest_size, src, src_size);
}

template<typename CharType, typename SizeType>
CharType const* StringUtility<CharType, SizeType>::SkipWhiteSpace(char_type const* str)
{
    return InternalSkipWhiteSpace<char_type>::SkipWhiteSpace(str);
}

template<typename CharType, typename SizeType>
template<typename NumericType>
bool StringUtility<CharType, SizeType>::ToNumber(NumericType& value,
                                                 char_type const* str,
                                                 int base)
{
    return InternalToNumber<char_type, NumericType>::ToNumber(value, str, base);
}

#if 0
template<typename CharType, typename SizeType>
template<typename NumericType>
bool StringUtility<CharType, SizeType>::ToString(char_type* /*str*/,
                                                 size_type /*str_size*/,
                                                 NumericType /*value*/)
{
    typedef InternalToString<CharType, NumericType, SizeType> internal_to_string_type;
    return false; // internal_to_string_type::ToString(str, str_size, value);
}
#endif
